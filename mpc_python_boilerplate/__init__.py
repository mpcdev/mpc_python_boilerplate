# -*- coding: utf-8 -*-

"""Top-level package for MPC Python Boilerplate."""

__author__ = """Matthew John Payne """
__email__ = 'matthewjohnpayne@gmail.com, mpayne@cfa.harvard.edu'
__version__ = '18.73.0'
