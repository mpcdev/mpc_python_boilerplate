======================
MPC Python Boilerplate
======================


.. image:: https://img.shields.io/pypi/v/mpc_python_boilerplate.svg
        :target: https://pypi.python.org/pypi/mpc_python_boilerplate

.. image:: https://img.shields.io/travis/matthewjohnpayne/mpc_python_boilerplate.svg
        :target: https://travis-ci.org/matthewjohnpayne/mpc_python_boilerplate

.. image:: https://readthedocs.org/projects/mpc-python-boilerplate/badge/?version=latest
        :target: https://mpc-python-boilerplate.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




MPC-Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://mpc-python-boilerplate.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
